#!/usr/bin/python
from __future__ import print_function
import boto3
from datetime import date, datetime, timedelta
import json
import time
import os
import smtplib
from botocore.exceptions import ClientError
from termcolor import colored, cprint

count = 1

# Boto connection

session = boto3.Session(profile_name='default')
ec2 = session.resource('ec2')


try:
        upperDate=datetime.now() - timedelta(days=7)
        # lowerDate is the release of Amazon DynamoDB Backup and Restore - Nov 29, 2017
        lowerDate=datetime(2017, 11, 29)
        nowDate=datetime.now()
        tomorrowDate=datetime.now() + timedelta(days=1)
        msg = '<table border="1" style="width:100%; border-collapse: collapse; border: 1px solid black; padding: 2px;"><tr style="background:#D3D3D3;color:black;"><th align="center" colspan="6">{0} - {1}-{2}-{3}</th></tr>\n'.format("Purple Strategies - Instance Status Report",nowDate.year,nowDate.month,nowDate.day)
        msg = msg + "<tr style='background:#e5e5e5'><th>{0}</th><th>{1}</th><th>{2}</th><th>{3}</th><th>{4}</th><th>{5}</th></tr>\n".format("S. No.","Public IP","Instance-Id","State", "Instance Type","Name")
        for i in ec2.instances.all():
                IIP = i.public_ip_address
                IID = i.id
                msg = msg + "<tr><td>{0}</td><td>{1}</td><td>{2}</td>".format(count,IIP,IID)

                IState = i.state['Name']
                if      IState == 'running':
                        msg = msg + "<td bgcolor='green' >{0}</td>\n".format(IState)
                else :
                        msg = msg + "<td bgcolor='red'>{0}</td>\n".format(IState)

                IIns = i.instance_type
                for idx, tag in enumerate(i.tags, start=1):
                        if tag['Key'] == 'Name':
                                ITag = tag['Value']
                ITagN = ITag
                msg = msg + "<td>{0}</td><td>{1}</td></tr>\n".format(IIns,ITagN)
                count = count + 1
        msg = msg + "</table><br>"

        # Send Mail
        mailto = 'rahul.netawat@diaspark.com'
        #mailto = 'server.support@webdunia.net'
        mailfrom = 'devops@diaspark.com'
        user = 'wdsmtp'
        pwd = 'auth@3434'
        smtpserver = smtplib.SMTP("65.182.162.96",2525)
        smtpserver.ehlo()
        smtpserver.ehlo() # extra characters to permit edit
        smtpserver.login(user, pwd)
        total = "<h2>Total Servers: {0}</h2>".format(count-1)
        header = 'MIME-Version: 1.0' + "\r\n";
        header = header + 'Content-type: text/html; charset=iso-8859-1' + "\r\n";
        header = header + 'To:' + mailto + '\n' + 'From: DevOps Diaspark <devops@diaspark.com>\r\n' + 'Subject:Purple Strategies - Instance Status Report\n'
        msg = msg = header + '\n' + total + msg + '\r\n'
        smtpserver.sendmail(mailfrom, mailto, msg)
        smtpserver.quit()

except  ClientError as e:
        print('client error: ',e)

except ValueError as ve:
        print('value error: ',ve)

except Exception as ex:
        print('exception: ',ex)


